/**
 * @author   Erick Garcia egarcia87@miners.utep.edu
 * @desc     This class will get an input from the user and tokenize it
 * @required egarcia_argtok.h
 */

#include "egarcia_argtok.h"

/**Func argtok 
 * @desc   Tokenize a string into a **char of *char
 * @param  c      = string we want to tokenize 
 * @return tokens = the tokenized string
 */
char** argtok(char* c){
  char** tokens = NULL;                                    //Create our array of char* pointing to char*
  int no_of_tokens = count_tokens(c);                      //Counting our tokens
  tokens    = (char**)calloc(no_of_tokens, sizeof(char*)); //Add one for the '\0'
  int index = 0;                                           //Index position in our **tokens
  int i     = 0;                                           //Loop iterator
  int chars = 0;                                           //Number of characters in our input
  /* Allocate memmory for each char* (word) and allocating the values */
  while(c[i] != '\0'){ 
    if(c[i] != ' ')
      chars++;                                      //Counting chars until empty space
    else{
      chars++;                                      //Extra count for the '\0'
      tokens[index] = malloc(chars*(sizeof(char))); //Allocate
      chars         = 0;                            //Going to start another token
      index++;                                      //variable to transverse our source string
    }
    i++;
  }
  chars++; 
  tokens[index] = malloc(chars*(sizeof(char))); 
  /* Preparing the itterators for the last loop */
  i     = 0;
  index = 0;
  int j = 0;
  /* Implementation of strcopy from source to dest */
  while(c[i] != '\0'){
    if(c[i] != ' '){
      tokens[index][j] = c[i]; //Copying from source to destination
      j++;
    }
    else{
      tokens[index][j] = '\0'; //If it reaches an empty space terminate token
      index++;
      j = 0;
    }
    i++;
  }
  tokens[index+1] = NULL; //Last Token with a NULL terminator
  return tokens;
}


/** func    count_tokens
 * @desc   Count the number of tokens in a string  
 * @param  c     = string we want to count tokens from
 * @return count = number of tokens counting space and '\0' 
 */
int count_tokens(char* c){
  int i = 0; 
  int count = 0;
  while(c[i] != '\0'){
    if(c[i] == ' '){
      count++;
    }
    i++;
  }
  return count + 2;
}

/* Print all of the tokens (words) in the char** */ 
/** func    count_tokens
 * @desc   Count the number of tokens in a string  
 * @param  c     = string we want to count tokens from
 * @return count = number of tokens counting space and '\0' 
 */
void print(char** tokens){
  int i, j;
  for(i = 0; tokens[i] != NULL; i++){      //If null we are done
    for(j = 0; tokens[i][j] != '\0'; j++){ //If '\0' skip a line
      printf("%c", tokens[i][j]);
    }
    printf("\n");
  }
}

/** func   freeMemory
 * @desc   Since we are using malloc/calloc we have to free memory
 * from the heap
 * @param  tokens = **char that we will free
 */
void freeMemory(char** tokens){
  int i;
  for(i = 0; tokens[i] != NULL; i++){      //If null we are done
    free(tokens[i]);
  }
  free(tokens);
}
