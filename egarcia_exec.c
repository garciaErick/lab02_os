/**
 * @author   Erick Garcia egarcia87@miners.utep.edu
 * @desc	   This class will manipulate all of the operations 
 * related to commands in our shell
 * @required egarcia_argtok.h egarcia_exec.h 
 */
#include "egarcia_exec.h"

/**
 * @desc   Determine whether this should run in the background
 * or not  and adapt our tokens to remove the '&'
 * @param  args = Our tokenized input
 * @return Return 1 or 0 if it should or should not run in the
 * background
 */
int execBackground(char **args) {
  int i = 0;
  // check for the ampersand at the end of the set of tokens
  while(args[i] != 0)       {
    i++;
  }                         // traverse to the end of the tokens
  if(args[i-1][0] == '&') { // check the last token
    free(args[i-1]);
    args[i-1] = NULL;       // remove the ampersand
    return 1;
  }
  else {
    return 0;
  }
}

/**
 * @desc   Determine whether this output should be written into
 * a file and adapt our tokens to remove the '>'
 * @param  args = Our tokenized input
 * @return Return the index of the '>' or 0 if it should or should 
 not write output into a file
 */
int redirectOutput(char **args){
  int i           = 0;
  int hasRedirect = 0;       //Flag to check if it has the '>'
  while(args[i] != 0)      { //Iterate through our tokens
    if(args[i][0] == '>'){   //If it has the '>'
      hasRedirect = 1;       //Set the flag to 1
      free(args[i]);         //Free the space
      args[i] = NULL;        //Clean up our tokens so it receives only the command
      return i;              //Return the position of the '>'
    }
    i++;
  }
    return 0;                //No redirects
}

/**
 * @desc   All the code for actually executing and calling the commands
 * @param  args = Our tokenized input
 * @return the status of our program
 */
int executeCmd(char **args) {
  int bg       = execBackground(args); //Checking if it has an '&'
  int redirect = redirectOutput(args); //Index of '>' if any
  pid_t pid;
  pid          = fork();               //Fork a child process
  if(pid < 0) {                        //Negative process id means there was an error
    fprintf(stderr,"Error forking a process\n");
    return -1;
  }
  else if(pid == 0) {                  //this is the child process run args command */
    if(redirect > 0){                  //There is indeed a redirect
      char* output = args[redirect+1]; //The token after the redirect is the name of the file
      /* The open() method is like creat() with the addition of following flags and permissions */
      /* int fd1 = creat(output, 0644);   // 0644 = -rw-r--r-- */
      int fd1 = open(output, O_CREAT|O_RDWR|O_TRUNC, 0644); 
      dup2(fd1, STDOUT_FILENO);        //Print output of command to file
      close(fd1);                      //Close
      execvp(args[0], args);
    }
    //Execute command "arg[0]" with flags "all of the tokens"
    execvp(args[0], args);
    //If its an invalid shell commmand
    fprintf(stderr, "My shell: Command not found\n");     
    exit(1); //Terminate Process
  }
  else {
    if(bg){                            //If it is a background process
      printf("[process %d started in the background]\n", pid); 
      /* sleep(3); */
    }
    else{
        /* wait(NULL);                    //Wait for child process */
        waitpid(pid, NULL, 0);
    }
  }

  return 0; /* this will invoke an exit() system call */
}
