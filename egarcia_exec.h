/* @author Erick Garcia egarcia87@miners.utep.edu */
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

/**
 * @desc   Determine whether this should run in the background
 * or not  and adapt our tokens to remove the '&'
 * @param  args = Our tokenized input
 * @return Return 1 or 0 if it should or should not run in the
 * background
 */
int execBackground(char **args);
/**
 * @desc   Determine whether this output should be written into
 * a file and adapt our tokens to remove the '>'
 * @param  args = Our tokenized input
 * @return Return the index of the '>' or 0 if it should or should 
 not write output into a file
 */
int redirectOutput(char **args);
/**
 * @desc   All the code for actually executing and calling the commands
 * @param  args = Our tokenized input
 * @return the status of our program
 */
int executeCmd(char **args);
