/* @author Erick Garcia egarcia87@miners.utep.edu */

#ifndef EGARCIA_ARGTOK_H_ 
#define EGARCIA_ARGTOK_H_
#include <stdio.h>
#include <stdlib.h>

/* argtok():       Argument tokenizer                   */
/* count_tokens(): Counting Number of Tokens (Words)    */
/* print():        Printing tokenized string            */
/* freeMemory():   Freeing dynamically allocated memory */

char** argtok(char*);
int    count_tokens(char* c);
void   print(char**);
void   freeMemory(char**) ;

#endif //EGARCIA_ARGTOK_H_ 

